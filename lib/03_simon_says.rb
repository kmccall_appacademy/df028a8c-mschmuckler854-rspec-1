def echo(string)
    string
end

def shout(string)
    string.upcase
end

def repeat(string, rep=2)
    final = string
    for i in 1...rep
        final += " #{string}"
    end
    final
end

def start_of_word(string, how_many)
    string[0...how_many]
end

def first_word(string)
    words = string.split(" ")
    words[0]
end

def titleize(string)
    words = string.split(" ")
    final = words[0].capitalize
    words.drop(1).each do |word|
        if word == "the" || word == "over" || word == "and"
            final += " #{word}"
            else
            final += " #{word.capitalize}"
        end
    end
    final
end

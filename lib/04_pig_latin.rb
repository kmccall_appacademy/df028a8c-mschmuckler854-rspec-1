def pig_latinize(word)
    vowels = ['a','e','i','o','u']
    if vowels.include?(word[0])
        "#{word}ay"
        else
        if !vowels.include?(word[0]) && !vowels.include?(word[1]) && !vowels.include?(word[2]) || word[1] == "q"
            "#{word[3..-1]}#{word[0..2]}ay"
            elsif !vowels.include?(word[0]) && !vowels.include?(word[1]) || word[0..1] == "qu"
            "#{word[2..-1]}#{word[0..1]}ay"
            else
            "#{word[1..-1]}#{word[0]}ay"
        end
    end
end

def translate(string)
    final = string.split(" ").map { |i| pig_latinize(i) }
    final.join(" ")
end

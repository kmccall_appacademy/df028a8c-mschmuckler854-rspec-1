def add(x, y)
    if (x + y).nil?
        0
        else
        x + y
    end
end

def subtract(x, y)
    x - y
end

def sum(arr)
    arr.inject(0, :+)
end

def multiply(*num)
    num.inject(:*)
end

def power(x, y)
    x ** y
end

def factorial(x)
    if x == 0
        1
        elsif x > 0
        x * factorial(x - 1)
        else
        x
    end
end
